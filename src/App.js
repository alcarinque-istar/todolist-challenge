import './App.css';
import { createContext, useState, useEffect } from 'react';
import Root from './routes/root';
import FilteredList from './routes/filtered';
import UnfilteredList from './routes/unfiltered';
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

export const TodolistContext = createContext(null);
const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    children: [
      {
        path: "/tags/:tagId",
        element: <FilteredList/>,
      },
      {
        path: "/",
        element: <UnfilteredList/>,
      }
    ],
  },
]);

function App() {
  const [list, setList] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    try {
      const storedList = JSON.parse(localStorage.getItem('todolist'));
      console.log('Got from local', storedList);

      if (storedList)
        setList(storedList);
    } catch (e) {
      console.log('Couldnt get data from localstorage');
    } finally {
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    if (!loading) {
      console.log('List stored');
      localStorage.setItem('todolist', JSON.stringify(list));
    }
  }, [list]);

  if (loading) {
    return (
      <h1>Loading...</h1>
    );
  } else {
    return (
      <TodolistContext.Provider value={{list, setList}}>
        <RouterProvider router={router}/>
      </TodolistContext.Provider>
    );
  }
}

export default App;
