import { Outlet } from 'react-router-dom';
import NewForm from '../components/newForm';
import Tags from '../components/tags';
import { TodolistContext } from '../App';

const Root = () => {
    return (
        <div>
            <h2>Input Form</h2>
            <NewForm/>
            <h2>List</h2>
            <Outlet/>
            <h2>Tags</h2>
            <Tags/>
        </div>
    )
}

export default Root;