import { TodolistContext } from "../App";
import Item from "../components/item";
import { useContext } from "react";

const UnfilteredList = () => {
    const {list} = useContext(TodolistContext);
    
    return (
        <div>
            {list.length === 0 ? 
                <div>No Items</div> : list.map((i) => {
                    return (
                        <Item key={i.id} data={i}/>
                    )
                })
                }
        </div>
    )
}

export default UnfilteredList;