import { TodolistContext } from "../App";
import Item from "../components/item";
import { useContext } from "react";
import { useParams } from "react-router-dom";

const FilteredList = () => {
    const {list} = useContext(TodolistContext);
    const { tagId } = useParams()
    
    return (
        <div>
            {list.length === 0 ? 
                <div>No Items</div> : list.filter((i) => i.tags.includes(tagId)).map((i) => {
                    return (
                        <Item key={i.id} data={i}/>
                    )
                })
                }
        </div>
    )
}

export default FilteredList;