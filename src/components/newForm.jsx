import { useState, useContext } from 'react';
import { TodolistContext } from '../App';

const NewForm = () => {
    const [newText, setNewText] = useState("");
    const [newTags, setNewTags] = useState([]);
    const {list, setList} = useContext(TodolistContext);
    const handleTextChange = (e) =>{
        setNewText(e.target.value);
    }
    const handleTagsChange = (e) =>{
        const text = e.target.value;
        const tags = text.trim().split(',').map((t) => t.trim());
        setNewTags(tags);
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        setList([
            ...list,
            {
                desc: newText,
                tags: newTags,
                done: false,
                id: new Date().getTime(),
            }
        ]);
        setNewText('');
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                Enter new item: &nbsp;
                <input type="text" value={newText} onChange={handleTextChange}/>
                <input type="submit" value="Add"/>
                <br/>
                <br/>
                New item tags: &nbsp;&nbsp;
                <input type="text" value={newTags} onChange={handleTagsChange}/>
            </form>
        </div>
    );
}

export default NewForm;