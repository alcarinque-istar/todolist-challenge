import { TodolistContext } from "../App";
import { useContext } from 'react';
import { Link } from 'react-router-dom';

const Item = (props) => {  
    const { data } = props;
    const { list, setList } = useContext(TodolistContext);

    const handleCheckboxChange = (e) => {
        const index = list.findIndex((i) => i.id === Number(e.target.name));
        const newList = list;
        newList[index] = {
            ...list[index],
            done: !list[index].done,
        };
        setList(newList);
    }

    return (
        <div>
            <input type="checkbox" name={data.id} defaultChecked={data.done} onChange={handleCheckboxChange}/>
            {data.desc}&nbsp;&nbsp;
            {data.tags.map((i) => 
                (<Link key={data.id+i} to={`/tag/${i}`}>{i}</Link>)
            
            )}
        </div>
    );
}

export default Item;