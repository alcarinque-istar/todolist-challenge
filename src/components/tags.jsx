import { TodolistContext } from "../App"
import { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

const Tags = () => {
    const {list} = useContext(TodolistContext)
    const [ta, setTags] = useState([]);

    useEffect(() => {
        const newTags = new Set();
        list.forEach((i) =>{
            i.tags.forEach((t) => {
                if (!newTags.has(t)) newTags.add(t);
            })
        })
        setTags(Array.from(newTags));
    }, [list]);
    

    return (
        <div>
        { ta.length > 0 ? 
            ta.map((t, index) => {
                return (
                    <Link key={index} to={`/tags/${t}`}>{t}&nbsp;</Link>
                )
            }) : 
            <div>No tags. Use the tag textbox</div>
        }
        </div>
    );
}

export default Tags;